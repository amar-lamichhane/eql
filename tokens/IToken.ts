export interface IToken {
    getValue(): string;
}