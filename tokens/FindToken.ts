import { Token } from "../states/tokenUtilities";
import { IToken } from "./IToken";

export class FindToken implements IToken {
    getValue(): string {
        return Token.Find
    }

}