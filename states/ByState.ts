import { AbstractState } from "./AbstractState";
import { ILexerState } from "./ILexerState";
import { SortIdentifierState } from "./SortIdentifierState";
import { Token, isValidToken } from "./tokenUtilities";

export class ByState extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
       if (isValidToken(token)) {
        throw new Error("Invalid token after By. Cannot use keyword after By state");
       }
       return new SortIdentifierState();
    }
    
}