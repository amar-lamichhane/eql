import { Token } from "./tokenUtilities";

export interface ILexerState {
    setNextSuggestions<T>(options: T[]): void;
    getNextStateForUpdate(token: Token | string): ILexerState;
}