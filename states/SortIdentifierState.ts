import { AbstractState } from "./AbstractState";
import { ILexerState } from "./ILexerState";
import { InState } from "./InState";
import { Token, checkTokenEquality } from "./tokenUtilities";

export class SortIdentifierState extends AbstractState  implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if (checkTokenEquality(token, Token.In)) {
            return new InState();
        }
        throw new Error("Invalid token after Sorted By " + token + ". Keyword required is in");
    }
    
}