import { AbstractState } from "./AbstractState";
import { AscendingState } from "./AscendingState";
import { DescendingState } from "./DescendingState";
import { ILexerState } from "./ILexerState";
import { Token, checkTokenEquality } from "./tokenUtilities";

export class InState  extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if (checkTokenEquality(token, Token.Ascending) || checkTokenEquality(token, Token.Descending)) {
            return checkTokenEquality(token, Token.Ascending) ? new AscendingState() : new DescendingState();
        }
        throw new Error("Invalid token after In state");
    }
    
}