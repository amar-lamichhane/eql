import { AbstractState } from "./AbstractState";
import { EndState } from "./EndState";
import { ILexerState } from "./ILexerState";
import { Token, checkTokenEquality, isValidToken } from "./tokenUtilities";

export class OrderState extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if (isValidToken(token)) {
            throw new Error("Invalid token fater order")
        }
        return new EndState();
    }
}