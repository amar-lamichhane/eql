import { AbstractState } from "./AbstractState";
import { ConditionalRHSState } from "./ConditionalRHSState";
import { ILexerState } from "./ILexerState";

export class NotState extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]): void {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        return new ConditionalRHSState();
    }
    
}