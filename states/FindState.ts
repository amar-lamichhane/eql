import { ILexerState } from "./ILexerState";
import { FindIdentifierState } from "./FindIdentifierState";
import { Token, isValidToken } from "./tokenUtilities";
import { AbstractState } from "./AbstractState";
export class FindState extends AbstractState implements ILexerState {
    getNextStateForUpdate(token: Token | string): ILexerState {
        if(isValidToken(token)) {
            throw new Error("Invalid Token. Keyword cannot be used as a search text after Find token");
        }
        return new FindIdentifierState();
    }

    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
}