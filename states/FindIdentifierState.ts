import { AbstractState } from "./AbstractState";
import { ILexerState } from "./ILexerState";
import { SortedState } from "./SortedState";
import { WhereState } from "./WhereState";
import { Token, checkTokenEquality, isValidToken } from "./tokenUtilities";

export class FindIdentifierState extends AbstractState implements ILexerState {
    getNextStateForUpdate(token: Token | string): ILexerState {
        // either where or any identifier is okay here
        if (checkTokenEquality(token, Token.Where) || checkTokenEquality(token, Token.Sorted)) {
            return checkTokenEquality(token, Token.Where) ? new WhereState() : new SortedState();
        } else if (isValidToken(token) && token.toLocaleLowerCase() !== Token.Where) {
            throw new Error("Invalid token after find");
        } else {
            return new FindIdentifierState();
        }
        
    }

    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    
}