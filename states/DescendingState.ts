import { AbstractState } from "./AbstractState";
import { ILexerState } from "./ILexerState";
import { OrderState } from "./OrderState";
import { Token, checkTokenEquality } from "./tokenUtilities";

export class DescendingState extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if (!checkTokenEquality(token, Token.Order)) {
            throw new Error("Invalid token after Descending")
        }
        return new OrderState();
    }
}