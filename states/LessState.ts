import { AbstractState } from "./AbstractState";
import { ILexerState } from "./ILexerState";
import { ThanState } from "./ThanState";
import { Token, checkTokenEquality } from "./tokenUtilities";

export class LessState extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]): void {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if (checkTokenEquality(token, Token.Than)) {
            return new ThanState();
        }
        throw new Error("Invalid token after less")
    }
}