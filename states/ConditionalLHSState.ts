import { AbstractState } from "./AbstractState";
import { ILexerState } from "./ILexerState";
import { IsState } from "./IsState";
import { Token } from "./tokenUtilities";

export class ConditionalLHSState extends AbstractState implements ILexerState {
    getNextStateForUpdate(token: Token | string): ILexerState {
        if (token.toLocaleLowerCase() !== Token.Is) {
            throw new Error("Invalid token expecting 'is' after conditional token");
        }
        return new IsState();
    }

    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    
}