import { AbstractState } from "./AbstractState";
import { ConditionalRHSState } from "./ConditionalRHSState";
import { ILexerState } from "./ILexerState";
import { LessState } from "./LessState";
import { MoreState } from "./MoreState";
import { NotState } from "./NotState";
import { ThanState } from "./ThanState";
import { Token, checkTokenEquality, isValidToken } from "./tokenUtilities";

export class IsState extends AbstractState implements ILexerState {
    getNextStateForUpdate(token: Token | string): ILexerState {
        //check for the Not or the identifier other than Not
        if (isValidToken(token)) {
            if (checkTokenEquality(token, Token.Not) || checkTokenEquality(token, Token.Less) || checkTokenEquality(token, Token.More)) {
                switch(token.toLocaleLowerCase()) {
                    case Token.Not: 
                        return new NotState();
                    case Token.Less:
                        return new LessState();
                    case Token.More: 
                        return new MoreState();
                }
            } else {
                throw new Error ("Invalid token after Is");
            }
        }
        return new ConditionalRHSState();
    }

    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
}