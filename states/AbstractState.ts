
export abstract class AbstractState {
    protected __value = "";
    get value() {
        return this.__value;
    }
    set value(val: string) {
        this.__value = val;
    }
}