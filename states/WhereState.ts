import { isLeftHandSideExpression } from "typescript";
import { ConditionalLHSState } from "./ConditionalLHSState";
import { FindState } from "./FindState";
import { ILexerState } from "./ILexerState";
import { Token, isValidToken } from "./tokenUtilities";
import { AbstractState } from "./AbstractState";

export class WhereState extends AbstractState implements ILexerState {
    getNextStateForUpdate(token: Token | string): ILexerState {
        if (isValidToken(token)) {
            throw new Error("Invalid token. Keyword cannot be used after token where");
        }
        return new ConditionalLHSState();
    }

    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
}