import { AbstractState } from "./AbstractState";
import { ILexerState } from "./ILexerState";

export class EndState extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        return null as any;
    }
    
}