import { AbstractState } from "./AbstractState";
import { FindState } from "./FindState";
import { ILexerState } from "./ILexerState";
import { Token } from "./tokenUtilities";

export class InitialState extends AbstractState implements ILexerState {
    getNextStateForUpdate(token: Token | string): ILexerState {
        if (token.toLowerCase() !== Token.Find.toLocaleLowerCase()) {
            throw new Error('Invalid token at ' + token)
        }
        return new FindState();
    }

    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }

}