import { AndState } from "./AndState";
import { SortedState } from "./SortedState";
import { ILexerState } from "./ILexerState";
import { OrState } from "./OrState";
import { Token, checkTokenEquality, isValidToken } from "./tokenUtilities";
import { AbstractState } from "./AbstractState";

export class ConditionalRHSState extends AbstractState  implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if (isValidToken(token)) {
            if (checkTokenEquality(token, Token.And) || checkTokenEquality(token, Token.Or) || checkTokenEquality(token, Token.Sorted)) {
                if (checkTokenEquality(token, Token.Sorted)) {
                    return new SortedState();
                }
                return token.toLowerCase() === Token.Or ? new OrState() : new AndState()
            } else {
                throw new Error("Invalid token after Conditional RHS token")
            }
        } else {
            throw new Error("Invalid token after the RHS statement following where")
        }
    }

}