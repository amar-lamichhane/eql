import { AbstractState } from "./AbstractState";
import { ConditionalRHSState } from "./ConditionalRHSState";
import { ILexerState } from "./ILexerState";
import { Token, isValidToken } from "./tokenUtilities";

export class ThanState extends AbstractState  implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if(isValidToken(token)) {
            throw new Error("Invalid token after than");
        }
        return new ConditionalRHSState();
    }
    
}