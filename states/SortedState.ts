import { ILexerState } from "./ILexerState";
import { ByState } from "./ByState";
import { Token, checkTokenEquality } from "./tokenUtilities";
import { AbstractState } from "./AbstractState";

export class SortedState extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if (!checkTokenEquality(token, Token.By)) {
            throw new Error("Invalid token after sorted");
        }
        return new ByState();
    }
    
}