import { AbstractState } from "./AbstractState";
import { ConditionalLHSState } from "./ConditionalLHSState";
import { ILexerState } from "./ILexerState";
import { Token, isValidToken } from "./tokenUtilities";

export class AndState extends AbstractState implements ILexerState {
    setNextSuggestions<T>(options: T[]) {
        throw new Error("Method not implemented.");
    }
    getNextStateForUpdate(token: string): ILexerState {
        if (isValidToken(token)) {
            throw new Error ("LHS token after Or cannot be one of the keywords");
        } else {
            return new ConditionalLHSState();
        }
    }
    
}