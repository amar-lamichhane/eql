import { ILexerState } from "./ILexerState";

const ValidTokens: any[] = []
export enum Token {
    Find = 'find',
    FindIdentifier = '___findIdentifier___',
    Where = 'where',
    ConditionalLHSIdentifier = '___conditionallhsidentifier___',
    ConditionalRHSIdentifier = '___conditionalrhsidentifier___',
    Is = 'is',
    Not = 'not',
    And = 'and',
    Or = 'or',
    Order = 'Order',
    Before = 'before',
    After = 'after',
    Sorted = 'sorted',
    By = 'by', 
    In = 'in',
    Ascending = 'ascending',
    Descending = 'descending',
    Less = 'less',
    Than = 'than',
    More = 'more',
    Identifier = '___identifier___',
    Initial = '__initial__'
}


function generateValidTokens() {
    ValidTokens.length = 0;
    for (let prop in Token) {
        const tk = Token as any;
        ValidTokens.push(tk[prop]);
    }
}

export function getValidTokenForState(state: ILexerState) {

}

export function isValidToken(token: string): boolean {
    return ValidTokens.indexOf(token.toLowerCase()) !== -1;
}

export function isIdentifier(token: string): boolean {
    return token === Token.Identifier;
}

export function isNotIdentifier (token: string): boolean {
    return !isIdentifier(token);  
}

export function checkTokenEquality(token1: any, token2: Token) {
    return token1.toLowerCase() === token2.toLocaleLowerCase();
}

generateValidTokens();