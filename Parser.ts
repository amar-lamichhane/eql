import { ExpressionTree } from "./ExpressionTree";
import { AbstractState } from "./states/AbstractState";
import { AndState } from "./states/AndState";
import { AscendingState } from "./states/AscendingState";
import { ConditionalLHSState } from "./states/ConditionalLHSState";
import { DescendingState } from "./states/DescendingState";
import { FindState } from "./states/FindState";
import { IsState } from "./states/IsState";
import { LessState } from "./states/LessState";
import { MoreState } from "./states/MoreState";
import { NotState } from "./states/NotState";
import { OrState } from "./states/OrState";
import { OrderState } from "./states/OrderState";
import { SortedState } from "./states/SortedState";
import { WhereState } from "./states/WhereState";

interface Output {
    columns: string,
    condition: any[]// {key: name, operator: '<', value: 'val', logicalOperator: ''}
    sort?: {key: string, order: 'asc'|'desc'}
}

export class Parser {
    constructor (protected expressionTree: ExpressionTree) {
    }

    parse(): Output {
        const ret: Output = {columns: '', condition: [], sort: undefined};
        // get the findIdentifiers
        const findState = this.expressionTree.Tree.find(e => e.root instanceof FindState);
        if (findState && findState.expressions.length > 0) {
            findState.expressions.forEach(e => {
                ret.columns += (e[0] as unknown as AbstractState).value + ' ';
            });
        }
        // Conditionals
        const conditionals = this.expressionTree.Tree.find(e => e.root instanceof WhereState);
        if (conditionals && conditionals.expressions.length > 0) {
            let condition = {key: '', operator: '', value: '',logicalOperator: ''};
            let logicalOperator: string|undefined = undefined;
            conditionals.expressions.forEach((element, i) => {
               const lhs = element[0] as unknown as AbstractState;
               const rhs = element[element.length - 1] as unknown as AbstractState;
               const elm = [...element];
               // removing the 0th and last item from the array because lhs and rhs are already referenced
               elm.shift(); 
               elm.pop();
               if (lhs instanceof OrState || lhs instanceof AndState) {
                    logicalOperator = lhs instanceof OrState ? 'Or' : 'And';
               } else {
                    condition.key = lhs.value;
                    condition.value = rhs.value;
                    if (logicalOperator != undefined) {
                        condition.logicalOperator = logicalOperator;
                        logicalOperator = undefined;
                    }
                    for (let i = 0; i < elm.length; i++)  {
                        if (elm[i] instanceof IsState) {
                            condition.operator = '=';
                        } else if (elm[i] instanceof NotState) {
                            condition.operator = '!=';
                        } else if (elm[i] instanceof MoreState) {
                            condition.operator = '>';
                        } else if (elm[i] instanceof LessState) {
                            condition.operator = '<';
                        }
                        // We don't need to check for than because More and Less will always have than key
                    }
               }
               if (condition.key !== '') {
                ret.condition.push(condition);
               }
               condition = {key: '', operator: '', value: '',logicalOperator: ''};
            });
        }
        // get sort values
        const orderStates = this.expressionTree.Tree.find(e => e.root instanceof SortedState);
        if (orderStates) {
            const sortions = orderStates.expressions[0];
            const [key, order] = sortions;
            ret.sort = {key: (key as unknown as AbstractState).value, 
                order: order instanceof DescendingState ? 'desc' : 'asc'
            };

        }
        return ret;
    }
}