/*

Find [value] where [something] is [value] or [something] is less than [value]

*/

import { AbstractState } from "./states/AbstractState";
import { FindState } from "./states/FindState";
import { ILexerState } from "./states/ILexerState";
import { InitialState } from "./states/InitialState";
import { Token, isValidToken } from "./states/tokenUtilities";
import { IToken } from "./tokens/IToken";
import { tokenFactory } from "./tokens/tokenFactory";



export class Lexer {
    private __state: ILexerState;
    private __tokens: IToken[] = [];
    constructor (private __input: string) {
        this.__state = new InitialState();
    }
    private __setState(state: ILexerState) {
        this.__state = state;
    }
    public process(): ILexerState[] {
        const init = new InitialState();
        const states: ILexerState[] = [init];
        this.__setState(init);
        // Splitting the string into array
        try {
            const inputArray = this.__input.split(' ').filter(e => e !== ' ');
            for (let i = 0; i < inputArray.length; i++) {
                const input = inputArray[i];
                const st = this.__state.getNextStateForUpdate(input);
                (st as unknown as AbstractState).value = input;
                this.__setState(st);
                states.push(this.__state);
            }
        }catch (e: any) {
            console.log(e.message);
            console.log(states);
        }
        return states;
    }
    
}