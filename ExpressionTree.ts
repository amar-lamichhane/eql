import { FindIdentifierState } from "./states/FindIdentifierState";
import { FindState } from "./states/FindState";
import { ILexerState } from "./states/ILexerState";
import { SortedState } from "./states/SortedState";
import { WhereState } from "./states/WhereState";
import { ConditionalLHSState } from "./states/ConditionalLHSState";
import { SortIdentifierState } from "./states/SortIdentifierState";
import { AndState } from "./states/AndState";
import { OrState } from "./states/OrState";
import { MoreState } from "./states/MoreState";
import { LessState } from "./states/LessState";
import { NotState } from "./states/NotState";

type IExpressionTreeNode = {
    root: ILexerState,
    expressions: Array<Array<ILexerState>>

}

export class ExpressionTree {
    private __tree: IExpressionTreeNode[] = []
    constructor (private __states: ILexerState[]) {
        this.createTree();
    }
    get Tree(): IExpressionTreeNode[] {
        return this.__tree;
    }
    private getTreeNodeOfType(t: any): IExpressionTreeNode | undefined {
        return this.__tree.find(e => {
            return e.root instanceof t
        })
    }
    private createTree() {
        for (let i = 0; i < this.__states.length; i++) {
            let e = this.__states[i];
            if (e instanceof FindState || e instanceof WhereState || e instanceof SortedState) {
                this.__tree.push({
                    root: e,
                    expressions: []
                });
            }
            if (e instanceof FindIdentifierState) {
                const node = this.getTreeNodeOfType(FindState);
                node?.expressions.push([e]);
            }
            
            if (e instanceof ConditionalLHSState) {
                const node = this.getTreeNodeOfType(WhereState);
                const arr: Array<ILexerState> = [e];
                let j = i;
                // the following state has to be is so we can push it right away
                j++;
                arr.push(this.__states[j]);
                // the next state can be either not or the rhs state
                j++;
                if (this.__states[j] instanceof NotState) {
                    arr.push(this.__states[j]);
                    j++;
                }
                
                if (this.__states[j] instanceof MoreState || this.__states[j] instanceof LessState){
                    arr.push(this.__states[j]);
                    // the next should be than state
                    j++;
                    arr.push(this.__states[j]);
                    j++;
                }
                // THe RHS state should be the last one to be added to the stack
                arr.push(this.__states[j]);

                node?.expressions.push(arr);
                // the following state could be either and or or state or non. So adjust the condition accordingly.
                if (this.__states[j+1] instanceof OrState || this.__states[j+1] instanceof AndState) {
                    node?.expressions.push([this.__states[j+1]]);
                    j++;
                }
                i = j;
            }

            if(e instanceof SortIdentifierState) {
                const node = this.getTreeNodeOfType(SortedState);
                const arr: Array<ILexerState> = [e];
                arr.push(this.__states[i+2]);
                node?.expressions.push(arr);
                i+=2;
            }
        }
    }
}